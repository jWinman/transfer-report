\section{Forward Plan}
Inspired by the results presented in this report, my future work will mainly be a continuation of the current projects.
The forward plans for three different projects will be described in this section.

The first one focusses on creating a new model for 2D wet foam, which combines the practicality of the soft disk model with the accuracy of the PLAT simulation.
The model will be based on the work by Morse and Witten \cite{MorseWitten} about the deformation of a bubble, subject to a point force.
In section \ref{sec:2Dforcemodel}, the main points of this theory for a 2D bubble and a rough algorithm will be explained.

The second project will involve more simulation of columnar structures of soft spheres from section \ref{sec:softspheres}.
This project has opened many paths that can be followed in the near future.
One could find new applications for the given simulations, or try to bring the simulations and experiment to better agreement.

The third project is in collaboration with the photographer \href{http://www.kymcox.com/}{K.~Cox} and A.~Kraynik, similar to the second one and concerns strings of monodisperse bubbles, which self-organises into columnar structure.
\href{http://www.kymcox.com/}{K.~Cox} identified similar structures to those in section \ref{sec:softspheres} for unconfined bubbles.
Preliminary simulations with the surface evolver were done by A.~Kraynik.
We will continue with this work and extend it to other structures identified by \href{http://www.kymcox.com/}{K.~Cox}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The 2D force model: Taking bubble deformation into account}
\label{sec:2Dforcemodel}

From section \ref{sec:2Dfoam}, we saw that the soft disk model is not in agreement with results from ideal 2D foam simulations, when it comes to properties such as the contact number, because it does not take account for the deformation of a bubble.
Thus, we want to develop a new foam model that incorporates it, but just as in the soft disk model, focusses on entire bubbles.
The new model will therefore combine the practicality from the soft disk model and the accuracy from PLAT.

\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=0 0 0 0, width=0.45\linewidth]{figures/bubble.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.05, 1.05) {\LARGE (a)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=0 0 0 0, width=0.45\linewidth]{figures/bubblePolar.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.05, 1.05) {\LARGE (b)};
\end{scope}
\end{tikzpicture}
\caption{
The adjustment of shape for a 2D bubble subject to a point force at $\theta = 0$ calculated with the Morse and Witten theory.
In (a) the outward radial displacement $\delta R(\theta)$ for different point forces $f = 0.1, 0.5, 1.0$ acting on the bubble is plotted.
(b) shows the radial distance $R(\theta)$ in a polar coordinate for different point forces $f = 0.1, 0.5, 1.0$ acting on the bubble.
The thick black line represents the undeformed circular bubble of same area with radius $R_0 = 1$. 
}
\label{fig:bubbleshape}
\end{figure}

From the work of Morse and Witten \cite{MorseWitten} we know how a bubble, subject to a point force $f$ in a compensatory gravitational field, adjusts its shape.
A closer look at the 2D case has been done by Weaire et al, who are currently preparing a publication on this matter.
In 2D, the outward radial displacement $\delta R(\theta) = R(\theta) - R_0$ of the bubble obeys the differential equation:
\begin{equation}
- \left(\frac{\dx^2}{\dx \theta^2} + 1 \right) \delta R(\theta) = A + \frac{f}{\pi} \cos\theta\,,
\label{eq:ode}
\end{equation}
where $R_0$, the radius of a circle having the same area as the bubble, is set to $R_0 = 1$ for simplicity.

The constant $A$ is determined by the condition that the area of the bubble remains fixed and the constants of integration are set by symmetry conditions at $\theta = \pi$.

The solution to \eqref{eq:ode} that obeys all conditions is
\begin{equation}
\delta R(\theta) = \frac{f}{2\pi} (\pi \sin\theta - \cos\theta - \theta \sin\theta) - \underbrace{\frac{f}{2\pi}}_{{} = A}\,.
\label{eq:solution}
\end{equation}

Fig. \ref{fig:bubbleshape} (a) shows a plot of the solution over the range $2 \pi$ for three different $f$.
By adding $R_0 = 1$ to $\delta R(\theta)$ one obtains the radial distance $R(\theta)$ of the deformed bubble.
A polar plot of this quantity is shown in Fig. \ref{fig:bubbleshape} (b).
For comparison, the circle for $R_0 = 1$ is plotted in black.

A simple test case for this model is constructed in figure \ref{fig:betweenPlates}, where a bubble is compressed between two plates with small deformation.
Since $\delta R(\theta)$ in equation \ref{eq:solution} depends linearly on the force, the shape of the bubble can be calculated by superimposing the adjusted shapes due to the two point forces.
The grey shaded area shows the solution with the Morse and Witten theory, which is in good agreement with the analytic solution, plotted as the solid line around it.
From the dashed circle, which represents the soft disk model, one can see why the soft disk model does not agree with the 2D foam regards variation of contact numbers.
Considering soft disks to the left and right, they would not get into contact, while the analytic solutions and the Morse and Witten theory would suggest the formation of new contacts.

Based on this theory, we will develop a simulation model that represents the foam by the centre points of each bubble and forces between each bubble.
Hence, the name of the simulation is chosen to be ``2D force model''.
The contacts are simply known by the repelling forces.

To find the wet foam structure at equilibrium an algorithm, similar to energy minimisation, is applied.
However, no information about the energy is needed because for a local minimum it is sufficient that the net force on one bubble vanishes.
Thus, the bubbles will be moved, so that the net forces on each bubble are zero. 

The algorithm will then be tested on different test cases where analytic solutions are known.
These test cases can reach from small systems with few bubbles under compression to ordered foam structures.
As a milestone, the 2D force model will be able to reproduce current results from the PLAT simulation.

\begin{figure}[h!]
\centering
\includegraphics[width=0.6\textwidth]{Figures/betweenPlate0_10.pdf}
\caption{
The soft disk, dashed circle, and the adjusted shape of a bubble between two plates with the Morse and Witten theory, grey shaded area, for small deformation.
The solid line around the grey shaded area shows the analytic solution for this problem.
One can clearly see that the soft disk model already shows huge deviations from the analytic solution for small deformation, since it does not take adjustment of shape into account.
The shape, calculated by the Morse and Witten theory, on the other hand, is in good agreement with the analytic solution.
}
\label{fig:betweenPlates}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Packings of confined soft spheres}
\label{sec:futuresoftspheres}
Initially, the project started in section \ref{sec:softspheres} can be continued by generating the complete phase diagram with all possible phyllotactic structures.
While the current phase diagram is only an extract that shows all the structures around the $(3, \bm{2}, 1)$ line slip, the complete phase diagram would show all structures between $1 < D / d < 2.7379$.
Above that value the character of the packings changes because they contain a sphere in the centre that does not touch the cylinder wall.
With the complete phase diagram given, experiments can be extended to do a systematic search for more line-slip structures.
However, experiments will be done by Dr. Benjamin Haffner and I will only be involved in consulting matters.
  
For a more accurate description of the given experiments, we can also conduct computational fluid dynamic (CFD) simulations.
The simulations, described here, and the experiments show a huge deviation in the shape of the bubbles (elliptical in experiment and spheres in the simulation).
From \cite{Tripathi} we know that this shape is due to hydrodynamic effects and depends on various parameters, i.e. the flow rate.
However, such CFD simulations are beyond the expertise of the Foams and Complex Systems Group and would be outsourced to a collaborating group.
I would stay involved in further active disussion with such a group.

We will also adjust our simulation to search for structures with minimal enthalpy in-between concentric cylinders.
Since line-slip arrangements have also been found in the past for columnar packings on the surface of a cylinder \cite{mughal2012dense, mughal2014theory}, similar results will be expected for soft spheres between concentric cylinders.
However, apart from the pressure and the diameter ratio, the structures will also depend on the diameter of the outer cylinder.
This feature might reveal new structures, not known from current simulations.

%The shape distortion due to flow, together with that due to the forces between bubbles, and hence the equilibrium structure itself, will be analysed in a subsequent paper, together with a wide range of observations. Those forces are only very crudely modelled in the present paper. The necessary theory of  drainage of stable structures of  wet foam has not previously been formulated.

\subsection{Surface Evolver: Unconfined columnar bubble structures}
Finally, a third project related to the second project will be started in the near future.
Preliminary work has been done so far by \href{http://www.kymcox.com/}{K.~Cox}, and A.~Kraynik.

\href{http://www.kymcox.com/}{Kym Cox}, a collaborating photographer from England, who takes pictures of foam and presents them at art exhibitions, found columnar structures, similar to those from soft sphere packings inside a cylinder, by pushing bubbles out of a hose.
Examples of these pictures are given in figure \ref{fig:SE} (a) and (c).
As seen from the pictures, these bubbles are not confined by a cylinder as in previous experiments, but nonetheless result in columnar structures.
Depending on the bubble size, different columnar structures were observed.

\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
 \includegraphics[width=0.18\textwidth, clip=true, trim=200 200 200 0]{Figures/Capture0170-520-Edit.jpg}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.05, 1.05) {\LARGE (a)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.16\textwidth]{Figures/4_bubbles_08_out.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.05, 1.05) {\LARGE (b)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
 \includegraphics[width=0.158\textwidth, clip=true, trim=280 0 500 150]{Figures/L1100149.jpg}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.05, 1.05) {\LARGE (c)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.19\textwidth]{Figures/422_2_inside}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.05, 1.05) {\LARGE (d)};
\end{scope}
\end{tikzpicture}

\caption{
The bamboo structure from experiment (a) and simulation (b) and the (4, 2, 2) structure in experiment (c) and simulation (d).
The simulation structure from (d), however, was only done inside a cylindrical confinement.
Experiments were done by K.~Cox and simulations carried out by A.~Kraynik.
}
\label{fig:SE}
\end{figure}

Preliminary simulations with the Surface Evolver (SE) \cite{brakke1992} were done by A.~Kraynik, when he was visiting of the group in September, 2016.
During this stay, I had the chance to follow how the simulations were performed.
Pictures of the simulations are shown in figure \ref{fig:SE} (b) and (d).
These simulations are based on minimising the surface of the bubbles and are therefore static.
The simple bamboo structure in (a) could already be reproduced by the SE.
More complicated structures, such as the $(4, 2, 2)$ structures were only achieved inside a cylindrical confinement.

For the near future, I will start working with the SE by taking part in the workshop ``Mechanics of Liquid and Solid Foams'' which include an introduction to the SE, lead by A.~Kraynik and Stelios Kyriakides.
After getting familiar with the SE, I will take over the preliminary work from A.~Kraynik and apply it to more unconfined columnar structure.

\subsection{Gantt chart}
\includegraphics[width=0.85\textwidth]{../../ganttchart/gantt}
