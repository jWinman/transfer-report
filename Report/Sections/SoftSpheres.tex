\section{Simulation and observation of line-slip structures in columnar structures of soft spheres}
\label{sec:softspheres}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The simulation set-up and routine: Minimisation of enthalpy $H$}
We begin by describing the model system.
We adopt the elementary approach of Durian's soft disk model in three dimensions for soft spheres \cite{durian1995foam}.
As described in the introduction, it consists of spheres of diameter $d$, whose overlap $\delta_{ij}$ leads to an increase in energy according to
\begin{equation}
E^S_{ij} = \frac{1}{2} k \delta_{ij}^2\,.
\end{equation}
The spring constant $k$ was set to 1 for simplicity.
Here the overlap between two spheres $i$ and $j$ is defined as $\delta_{ij} = \vert \vec{r}_i - \vec{r}_j \vert - d$, where $\vec{r}_i=(r_i, \theta_i, z_i)$ and $\vec{r}_j=(r_j,\theta_j, z_j)$ are the centres of two contacting spheres in cylindrical polar coordinates. A similar harmonic energy term
\begin{equation}
E^B_i=((D/2 - r_i)-d/2)^2
\end{equation}
accounts for the overlap between the $i$th sphere and the cylindrical boundary.
We conduct simulations using a simulation cell of length $L$ and volume $V=\pi(D/2)^2L$.

On both ends of the simulation cell we impose \emph{twisted} periodic boundary conditions \cite{mughal2012dense}.
These are implemented by adding ghost spheres at the top and bottom of the simulation cell.
These spheres are image particles of spheres inside the simulation cell and therefore have the same radial component, but their $z$ component is shifted by $L$ and the angular component $\theta$ is changed by a twist angle $\alpha$.
They only contribute to the overlap energy with spheres in the simulation cell $E_{ij}^S$, where $i$ is a ghost sphere and $j$ a sphere inside the simulation cell.

Stable structures are found by minimising the enthalpy $H = E + pV$ for a system of $N$ soft spheres in the unit cell, where $E$ is the internal energy due to overlaps $E^S$ and $E^B$ as described before,
\begin{align}
H(\{\vec{r}_i\}, L) &= \sum_{i = 0}^{N} \sum_{j = i}^{N} E^s_{ij} + \sum_{i = 0}^N E_i^B + pV \\
&= E + pV\,.
\end{align}
A part of the resulting phase diagram is shown in  figure \ref{phasediagram}, where for a given value of the diameter ratio $D/d$ and pressure $p$ the enthalpy is minimised by varying the sphere centres, as well as the twist angle and the volume of the simulations cell. This is done for some values of $N$ and the structure with the lowest enthalpy is chosen.
 
For low pressures the minimisation was performed with a general minimisation algorithm called Basin-Hopping \cite{R144}.
This is a stochastic, global minimisation algorithm similar to simulated annealing, which was used in \cite{mughal2012dense} for the calculation of hard sphere packings.
The iterative algorithm consists of a cycle of three steps: random perturbation of the coordinates, local minimisation with a conjugate gradient technique, and acceptance or rejection of the new coordinates.
For the acceptance step the Metropolis criterion was used.

We found that the results from these preliminary simulations could be used as an initial guess for a much simpler code to map out the higher pressure regions of the phase diagram.
Here, the L-BFGS-B algorithm (named after Broyden, Fletcher, Goldfarb, Shanno on boundary constraints) \cite{R165} was used because it turned out to be the most efficient algorithm for our purpose.
It is a technique based on the conjugate gradient method that only uses first derivatives and is optimised compared to conjugate gradient, since it only searches for a minimum for a set of given constraints.
 
Starting with an initial structure (with $N = 3, 4, 5$) we steadily increased the pressure and minimised the enthalpy. As a further check we also ran the procedure in the orthogonal direction - i.e. we start with a seed structure with a high value of  $D/d$,  keep the pressure constant while reducing $D/d$ in discrete steps and minimise the enthalpy at each step.
The structure with the lowest enthalpy for a given value of $D / d$ and $p$ is selected from the two procedures and given in the phase diagram.
%In either case the structure with the lowest enthalpy for a given value of  $D / d$ and $p$ is given in the phase diagram.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The phase diagram around the $(3, 2, 1)$ line-slip structure}

\begin{table}[ht]
\centering
\captionsetup{justification=centering, font=small }
\caption{Hard Sphere Packings}
 %\begin{tabular}{ | l ||  p{1.25cm}| p{2.5cm}|}
 \begin{tabular}{l | p{2cm} p{2.5cm}}
 \toprule
 Range & Notation & Description\\ \colrule
%  $\;\;\;\;2.0 < D/d \leq 2.039$  & (2,{\bf 2},0)  & {\bf line-slip} \\ \hline
\rowcolor{Gray1}
  $D/d = 2.039$  & (3,2,1)  & uniform \\ \colrule
\rowcolor{Gray2}
  $\;\;\;\;2.039 \leq D/d \leq 2.1413$   & (3,{\bf 2},1)  & {\bf line slip}\\ \botrule \toprule
\rowcolor{Gray2}
  $\;\;\;\;2.1413\leq D/d < 2.1545$   & ({\bf 3},2,1)  & {\bf line slip}\\ \colrule
\rowcolor{Gray1}
  $D/d=2.1547$  & (3,3,0)  & uniform \\ \colrule
 \rowcolor{Gray2}
  $\;\;\;\;2.1547< D/d \leq 2.1949$  & (3,{\bf 3},0)  & {\bf line slip} \\ \botrule \toprule
 \rowcolor{Gray2}
  $\;\;\;\;2.1949\leq D/d \leq 2.2247$  & (3,{\bf 2},1) & {\bf line slip} \\ \colrule
\rowcolor{Gray1}
  $D/d = 2.2247$  & (4,2,2) & uniform \\ \botrule
\end{tabular}
\captionsetup{justification=centerlast, font=footnotesize}
\bigskip
\caption*{Partial sequence of densest uniform hard sphere packings, together with the line-slip structure, into which they may be transformed (i.e. $p=0$), adapted from Table I of \cite{mughal2012dense}.}
\label{exp:vf_table}
\end{table}

We plot the phase diagram for the range $2.00 \leq D/d \leq 2.22$ and pressures below $p \leq 0.02$, as shown in figure \ref{phasediagram}. Along the horizontal axis (i.e. $p=0$) the red diamonds indicate the symmetric hard sphere structures $(3,2,1)$, $(3,3,0)$ and $(4,2,2)$. Intermediate between them are the associated line-slip arrangements (see also  Table \ref{exp:vf_table}). The general trend is that increasing the pressure gradually eliminates the line slips which end up in a triple point and are forced into uniform structures. This transformation equates to a $\SIrange{10}{15}{\percent}$ compression of the hard sphere packings, in the range shown.

We now discuss the features of the phase diagram in detail. Continuous transitions, where the structure changes gradually due to the loss or gain of a contact, are shown in figure \ref{phasediagram} as dashed lines. Discontinuous transitions, where the structure changes abruptly into another, are indicated with a solid line. Transitions between uniform structures and a corresponding line slip are continuous. This is echoed by the sequence of hard sphere packings (i.e. the structures along the horizontal axis) as listed in Table \ref{exp:vf_table}.
In the table the abrupt changes in the hard sphere packings are illustrated with a double line.
Further explanation for hard sphere packings can be found in \cite{mughal2012dense}.
%An explanation for the connections between a symmetric structures and a line slip for the hard sphere packing can be found in figure 10 and 12 of reference \cite{mughal2012dense}. 

A typical example of a discontinuous transition is given by the solid line separating the $(3, \bm{3}, 0)$ and $(3, \bm{2}, 1)$ line-slip regions. We find in the case of soft spheres that with increasing pressure the  $(3, \bm{2}, 1)$ line slip is first to disappear at a triple point followed by the  $(3, \bm{3}, 0)$ line slip at a slightly higher pressure. At still higher pressures only the $(3,3,0)$ and $(4,2,2)$ uniform structures remain stable, separated by a discontinuous phase transition.

At first glance, the case $(3,2,1) \rightarrow (3,3,0)$  seems to be exceptional, in that only a single line slip, the $(3, \bm{2}, 1)$ line slip, is visible. From Table \ref{exp:vf_table} we would also have expected to see two line slips separating the uniform structures (as in the previous case). However, the  line-slip structure of the $(\bm{3}, 2, 1)$ exists only in the narrow range $2.1413 \leq D/d \leq 2.1545$ for the hard spheres (see  Table \ref{exp:vf_table}) and is not resolved in figure \ref{phasediagram}.


\begin{figure}[h!]
\centering
\includegraphics[width=0.65\columnwidth]{phasediagramContour.pdf}
\caption{
The form of the phase diagram for the range $2.00 \le D / d \le 2.22$ and pressures $p \le 0.02$.
Eight different structures are in the plotted range.
%With increasing pressure the widths of the line slips decrease and end in triple points.
Discontinuous transitions are displayed as solid black lines and continuous transitions are represented as dashed lines.
The diamond symbols at $p = 0$ correspond to the hard sphere symmetric structures \cite{mughal2012dense}.
A rough estimate of the experimentally observed line-slip structure is indicated by a circle on this phase diagram. \\
$^*$Note that there is also a very small $(\bm{3}, 2, 1)$ line slip region just below $D / d = 2.15$ \cite{mughal2012dense}, not resolved in the present simulation.
}
\label{phasediagram}

\includegraphics[width=0.65\columnwidth ]{Enthalpy.pdf}
\caption{
The enthalpy $H = (E + pV)$ as $D / d$ is varied at a constant pressure $p = 0.01$. The plot shows a horizontal cut through the phase diagram of figure \ref{phasediagram}.
}
\label{enthalpy}
\end{figure}

The distinction between continuous and discontinuous transitions can be illustrated directly via the enthalpy $H$. An example of $H$ in terms of $D / d$ at constant pressure $p = 0.01$ is given in figure \ref{enthalpy}.
Continuous transitions, such as $(3, 2, 1)$ uniform (indicated in purple) to $(3, \bm{2}, 1)$ line slip (indicated in blue), are not apparent in the variation of $H$. However, discontinuous transitions such as the $(3, \bm{2}, 1)$ line slip to $(3, 3, 0)$ uniform (green)  show a change in slope of $H$ at the transition.

The investigation of the complete phase diagram for higher $D / d$ up to $D / d \leq 2.7379$, beyond which the nature of the hard sphere packings changes \cite{mughal2012dense, fu2016hard}, and to higher pressure, will be part of our future plans (see also section \ref{sec:futuresoftspheres}).
%In due course we shall use the methods developed here to investigate the phase diagram for higher $D/d$ up to $D / d \leq 2.7379$, beyond which the nature of the hard sphere packings changes \cite{mughal2012dense, fu2016hard}, and to higher pressure.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The $(3, 2, 1)$ line slip in an experiment}
\begin{figure}[h!]
\centering
\includegraphics[width=0.4\columnwidth, clip=true, trim=0 0 15 0]{expts.png}
\caption{
Observation of columnar structures of bubbles under forced drainage, showing about forty bubbles.
Left: $(3,2,1)$ uniform structure occurring at a relatively low flow rate.
Right:  Bubbles adopt the $(3,\bm{2},1)$ line-slip structure at a relatively high flow rate.
It is similar to the line-slip structure of figure (\ref{bubblespheres}b). The  coloring highlights the two interlocked spirals of the (3,2,1) and $(3,\bm{2},1)$ structures. The circle marked on figure \ref{phasediagram} corresponds roughly to its estimated position on the phase diagram. 
}
\label{bubblespheresx}
\end{figure}

We now describe our experimental procedure and results.
All experiments, described here, has been performed by Dr. Benjamin Haffner, Post-doc in the Foams and Complex Systems Group.
He identified and analysed the experimental structures together with me, since I have the expertise in categorising and labelling the columnar structure.

We choose to observe structures of columns of bubbles under \emph{forced drainage} that is, a steady input of liquid from above \cite{Pardal1993, weaire1997review, koehler2000generalized}. In the past this has been extensively studied for columns of bulk foam where $D / d$ was very large. It was found to undergo convective instability for flow rates that gave rise to higher liquid fractions, confining the experiment to relatively dry foam \cite{Crawford1998}. The corresponding theory was therefore developed using approximations appropriate in the dry limit, that is, in terms of flow in a network of narrow Plateau borders.

The present work leads us to consider columns of wet foams, where the drainage rate high enough to produce near-spherical bubbles.
We find no such instability in the case of the confined columnar structures of large bubbles considered here; 
hence the wet limit can be reached at a certain flow rate.
Below that point, the liquid fraction can be varied, producing ordered columnar structures closely analogous to those described above.

For the experimental set-up \cite{Pardal1993, hutzler1997moving}, the monodisperse bubbles are produced by using a bubbling needle in a commercial surfactant Fairy Liquid. The surfactant solution contains $\SI{50}{\percent}$ of glycerol in mass to increase the viscosity, smooth the structure transitions and allow the observation of more easily unstable bubble arrangements.
Then, the bubbles arrange into ordered structures in a vertical glass tube for which the diameter is $\SI{5}{\milli\meter}$ and the length is $\SI{1.5}{\meter}$.
A pipette on the top of the column allows us to feed the foam with a surfactant solution and to reduce the pressure within the glass tube by increasing the flow rate.

We show in figure \ref{bubblespheresx} an example of the observation of the $(3, \bm{2}, 1)$ line-slip structure in accord with expectations based on the soft sphere model, described above. Note that the bubbles are distorted into smooth ellipsoids by the flow.
The extent of the loss of contact in the observed line-slip structure is roughly equal to that of the simulated structure at the position marked with a circle in the phase diagram. This is also consistent with our estimates of equivalent pressure $p$ and equivalent $D / d$, although the latter is yet clear on account of the oblate shape of the bubbles.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Conclusion}
Considering that our original study of hard-sphere arrangements in a cylinder was motivated as a packing problem \cite{mughal2011phyllotactic}, it may have seemed plausible that the structures described above would only be of importance close to the incompressible athermal limit. Recently, however, these original findings have been found to be relevant in the \emph{self-assembly} of hard spheres in cylindrical channels \cite{fu2016assembly}. Remarkably, it is found that at finite temperature and pressure both uniform and line-slip arrangements can be observed: a steadily increasing pressure can induce transitions between structures, while at a high compression rate some arrangements are found to be dynamically inaccessible  \cite{fu2016assembly}, these results are illustrated in terms of a phase diagram that bears a resemblance to the results presented here. This is due to the fact that the thermally agitated hard spheres have an effective radius that scales with the mean free path, this leads to an effective interaction reminiscent of a soft potential. As such, these results only indirectly hint at the manner in which the hard sphere results are modified in the presence of soft interactions. The present results have the advantage of directly addressing the stability of line-slip arrangements in softly interacting systems.

Another related study is that of Rivier and Boltenhagen \cite{Boltenhagen1996} who observed structural transitions in cylindrical \emph{dry} foam under pressure, well outside of the range of our investigation.

In conclusion, questioning the relevance of hard-sphere properties to physical systems, consisting of \emph{soft} spheres has led us into a new unexplored territory, not previously explored.
We have shown that line-slip arrangements are a feature of soft systems (even in the absence of thermal agitation), thus extending their usefulness to encompass a range of commonly encountered substances including dusty plasmas, foams, emulsions and colloids. It is of direct relevance to some physical systems (foams, emulsions) and offers qualitative interpretation in others.
It extends into another dimension (that of pressure) the elaborate table of hard-sphere structures previously found.