\section{Introduction and previous work}%(10 pages max)

\subsection{Wet foam and other applications of soft objects}

\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=0 76 0 0, width=0.4\linewidth]{Figures/Guinness}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.05, 1.05) {(a)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=0 0 250 0, width=0.4\linewidth]{Figures/emulsion1}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.05, 1.05) {(b)};
\end{scope}
\end{tikzpicture}

\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=0 0 0 0, width=0.8\linewidth]{Figures/People2}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.05, 1.07) {(c)};
\end{scope}
\end{tikzpicture}


\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=0 0 0 0, width=0.8\linewidth]{Figures/People3}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.05, 1.07) {(d)};
\end{scope}
\end{tikzpicture}
\caption{
Examples for systems of soft particles.
Microscopic pictures of a wet foam of a Guinness beer in (a) and droplets of an emulsion in (b) and  (credit: Gavin Ryan, TCD Foams group).
Both show a typical spherical/circular shape due to the minimal surface area.
An illustration for the packing of people in a rectangular confinement for an ordered (zig-zag) structure is given in (c) and for an unordered structure in (d).
The pictures were taken during a student project in front of the SNIAM building.
}
\label{fig:examples}
\end{figure}

People jammed into over-crowded buses, huddling Emperor penguins, tins filled with peeled tomatoes, droplets in an emulsion, and platelets in blood vessels show that dense agglomeration of soft objects occur on many length scales and in many different contexts.
Wet foams, i.e. dense packings of gas bubbles in a liquid, are just one example of such a system.

Several examples of soft objects are illustrated in figure \ref{fig:examples}.
Figure \ref{fig:examples} (a) shows the ``classical'' system of a wet foam from a Guinness beer.
In addition the agglomeration of droplets in emulsions (b) and packing of people in a rectangular confinement (c) and (d) can also be considered as soft objects, similar to those of wet foam.

The packing of people is a rather unconventional application of such systems.
But as figures \ref{fig:examples} (c) and (d) show, the packings show a similar ordered (c) and disordered (d) behaviour to wet foams or emulsions.
People seem to arrange in an ordered packing for small rectangular widths and introduce disorder as when the width is increased.
We will see that ordered structure similar to (c) also appear in confined packing of soft spheres.

Jamming of soft particles can have dramatic implications, for example in the case of evacuation of a building in an emergency, or the formation of a blood clot.
We have focussed in this work on the theoretical understanding of generic properties of such aggregations, based on computer simulations of analogous systems.

Bubbles are well approximated as deformable, frictionless objects.
The energy associated with their packing is surface energy, making foams structures of minimal surface area.
Foams are also of interest in their own right; familiar from everyday household experience, they feature in many large scale industrial operations, such as mineral processing (flotation) or paper making \cite{PaperMaking1}.
An understanding of their properties, for example their rheological properties, requires knowledge of their structure, in particular as a function of liquid fraction.

Current knowledge is restricted mainly to dry foams (liquid fraction less than about $\SI{5}{\percent}$) which form a polyhedral cellular structure, and foams close to a critical liquid fraction, which may be approximated as sphere packings as seen in figure \ref{fig:examples} (a).
We have investigated various properties of wet foam structures between these two limits with computer simulations.

In particular, two subjects of interest have been investigated:
Firstly, we examined the variation of the average contact number, $Z$, for 2D wet foam above the jamming transition, the point where the bubbles jam when being compressed \cite{ohern2003jamming}.
By comparing our 2D foam simulation with Durian's soft disk simulation, a simple model for wet foam (for more explanations see section \ref{sec:durian}), we demonstrate the limits of the latter one.

Secondly, computer simulations of soft spheres in cylindrical confinements were conducted.
The resulting structures were reproduced with experimental observations of columnar foam structures.
Both strands of research have in common that they both concern the structure of dense packings of soft deformable particles.

\subsection{Durian's soft disk model}
\label{sec:durian}

The soft disk model was first introduced by Durian in 1995 \cite{Durian95}.
While other models, especially those for dry foams, simulate foam by films or vertices, the soft disk model focuses on entire bubbles, which are approximated by disks and can overlap with each other.
This property makes the soft disk model easy to implement and efficient to run time.
A typical disordered packing from the soft disk model is illustrated in figure \ref{fig:durian}.
One can see that the disks in 2D do not deform or change their shape upon contact, rather they overlap.

Such packings are generated by finding the minimal energy configuration of such a system.
In the soft disk model the interaction between two disks is model by a harmonic repelling spring.
Thus, the interaction energy between the $i$th and the $j$th disk is proportional to the square of the overlap $\delta_{ij}$.
The total energy is then
\begin{equation}
E = \frac{1}{2} \sum_{i = 0}^N \sum_{j = i}^N k \delta_{ij}^2\,,
\end{equation}
with the spring constant $k$.
The minimisation to obtain the disordered packing is achieved using the conjugate gradient method.
Thus, this method can be used to simulate jamming of frictionless granular materials.

\begin{figure}[t]
\centering
\includegraphics[width=0.35\textwidth]{figures/Packing09.pdf}
\caption{
A typical packing from Durian's soft disk model at a packing fraction of $\phi = 0.90$.
The disks overlap upon contact and do not deform or change their shape.
}
\label{fig:durian}
\end{figure}

Despite, or because of its simplicity, this model has become the most commonly used tool to simulate foam since its introduction.
In fact, due to its good agreement in reproducing the Herschel-Bulkley type rheology, which is associated with emulsions and foams, people became accustomed to believe that it is a good representation of a 2D foam \cite{LangloisEtal08}.
Since the shape of the bubbles are in fact disks at the critical gas fraction (or packing fraction in case of disk packing) $\phi_c$, this may also be used at and as a first approximation above $\phi_c$.
However, its limits above $\phi_c$, towards foam-like systems are obvious.
The disks that represent bubbles do not deform upon compression, they only overlap, while bubbles in a foam change their shape.
Also, these overlaps reduce the bubble area, which is conserved in a 2D foam. 

With the introduction of the model \cite{Durian95}, Durian found that the average contact number $Z$ varies as the square root of the packing fraction (or gas fraction in case of foams) $\phi$
\begin{equation}
Z - Z_c \propto (\phi - \phi_c)^{1/2},
\label{e:square-root}
\end{equation}
where $\phi_c$ is its critical value.
The contact number can be calculated by counting the total number of contacts in the system devided by the number of disks or bubbles.
In what follows, the terms gas fraction and packing fraction are used interchangeably.

In the limit of an infinite system the critical contact number at $\phi_c$ is $Z_c = 4$.
This can be explained by arguments, based on counting contraints \cite{maxwell1864,bennett1972serially, vanHeckePRL}.
While local stability requires at least {\em three} neighbours for each disk, overall stability requires {\em four} as an average \cite{maxwell1864, bennett1972serially, vanHecke2010}. 

Since then further jamming simulations using the same model with different parameters \cite{ohern2003jamming}, but also various experiments for quasi-2D foams \cite{KatgertVanHecke2010} and 2D elastic disks \cite{MajmudarEtal2007}, have been in agreement with Durian's finding.

Surprisingly, the variation of the average contact number for an ideal 2D foam are different.
In section \ref{sec:2Dfoam}, we present our results and set them into context with the previous findings.
We also give a consistency argument that supports our surprising findings. 

\subsection{Categorisation of columnar packings}

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\columnwidth, clip=true, trim=15 0 0 0]{hard_sphere_structures.jpeg}
\caption{Examples of a columnar hard sphere structures, (a)  a uniform arrangement $(3, 2, 1)$; and, (b) a related line-slip structure $(3, \bm{2}, 1)$.
The gaps in (b) correspond to a loss of contact compared to (a). }
\label{bubblespheres}
\end{figure}

Columnar structures are ubiquitous throughout biology: examples range from viruses, flagella, microtubules, microfillaments, as well as certain rod shaped bacteria \cite{erickson1973tubular, hull1976structure, brinkley1997microtubules, bryan1974microtubules, amir2012dislocation}.
However, they are also increasingly recognised in the physical sciences, particularly in nano-physics \cite{smalley2003carbon, chopra1995boron, brisson1984tubular} in the form of nanotubes and nanowires. Other notable examples include the helical self-assembly of dusty plasmas \cite{tsytovich2007plasma}, asymmetric colloidal dumbbells \cite{zerrouki2008chiral} and particles trapped in channels \cite{yin2003self}. More recently such columnar arrangements have also been found as components of exotic crystal structures \cite{douglass2017stabilization, wang2016electric}.

Such columnar structures arise in their most elementary form when we seek the densest packing of hard spheres inside (or on the surface of) a circular cylinder \cite{mughal2011phyllotactic, chan2011densest,  mughal2012dense, mughal2013screw, yamchi2015helical, fu2016hard, mughal2014theory}.
While soft spheres allow overlaps, hard spheres are non-deformable object that do not overlap. 
A wide range of these structures have been identified and tabulated for hard spheres \cite{mughal2012dense}, depending on the ratio of cylinder diameter $D$ to sphere diameter $d$.
For each of a set of discrete values of $D/d$, a symmetric structure is found, and may be labelled with the phyllotactic indices $(l,m,n)$ \cite{mughal2011phyllotactic, mughal2012dense}.

Each index represents the number of spirals that can be identified in a certain direction of the hexagonal packing.
For the given $(3, 2, 1)$ packing in figure \ref{bubblespheres} (a) the two coloured spirals represent the second phyllotactic index.
The three spirals for the first index can be seen when following the spheres from the bottom right to the top left and the spiral for the last index is on the horizontal line around the cylinder.
We can identify the following rule for each structure labelled by $(l, m, n)$:
The last two indices have to add up to the first one, $l = m + n$.

Between these values, the structure is best accommodated by the introduction of a \emph{line slip}, which shears two adjacent spirals with a loss of contacts. 
These features were first identified by Pickett et. al. \cite{Pickett2000}, but not termed ``line slip''.
In our example from figure \ref{bubblespheres} the first spirals from the second index are sheared from each other.
Thus, we denote the second index in bold.
One can clearly see the loss of contact in figure \ref{bubblespheres} (b), denoted as gaps.

Figure \ref{fig:hardspherePhi} shows previous results about the best packing fraction $\phi$ for various diameter ratios $D / d$, where $D$ is the cylinder diameter and $d$ the diameter of the hard sphere.
Each spike in this diagram corresponds to a symmetric structure and the line slips are the structures with lower packing fractions in-between.
There are always two or less line slips between two symmetric structures.
Each of them correspond to one symmetric structure.
%When following the line slips between two symmetric structures in this diagram, i.e. from $D/d \approx 2.15$ to another, $D / d \approx 2.23$, one can see an abrupt change in slope in the middle.
%However, each line slip starts at a symmetric structure and ends at another one.
By convention they are labelled by the symmetric structure with lower $D / d$.

\begin{figure}[h!!]
\centering
\includegraphics[width=0.6\textwidth]{figures/hardspherePhi.png}
\caption{
The optimal packing fraction $\Phi$ for varying diameter ratio $D / d$.
The spikes correspond to the symmetric structures, while the connecting lines are the packing fractions for line slips.
The inset are continuations of the same graph showing the regions $1 < D/d < 1.8$ (left) and $2.71486 < D/d < 2.873$ (right).
Structures above D = 2.71486 include internal spheres while those below do not; the division between these two regions is denoted by a heavy blue dashed line.
}
\label{fig:hardspherePhi}
\end{figure}

We have become accustomed to thinking of line slips as being a property of \emph{hard} sphere packings, and therefore of limited relevance to real physical systems such as foams or emulsions.
That point of view is reconsidered here: we demonstrate the existence of line-slip arrangements in soft particle systems (as described in section \ref{sec:softspheres}).

We first numerically investigate the stability of structures formed by soft repelling spheres in cylindrical channels subject to an applied pressure.
Our approach enables us to partially map out a rich phase diagram, consisting of a sequence of continuous and discontinuous transitions between uniform structures and line-slip arrangements. We show that in such soft systems line slips gradually vanish with an increasing pressure, while at high pressures only uniform structures remain stable.

Our work is stimulated in part by the observation of a line slip in an experiment with a wet columnar foam structure, carried out in the TCD group.
The results presented below improve on our previous attempts with small bubbles in capillaries under gravity, which proved difficult and in which line slips were not clearly seen \cite{meagher2015experimental}.
Our observations constitute the first conclusive experimental evidence of such structures (discounting the trivial case of packing ball-bearings in tubes \cite{mughal2012dense}).
Furthermore, our experiments with foams demonstrate that line-slip structures can be stable in \emph{soft} systems, a hitherto unexpected outcome.